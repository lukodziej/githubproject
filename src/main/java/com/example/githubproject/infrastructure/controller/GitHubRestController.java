package com.example.githubproject.infrastructure.controller;

import com.example.githubproject.domain.model.GitHubResultDatabaseList;
import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.proxy.dto.AllInfo;
import com.example.githubproject.domain.proxy.dto.GitHubListAllResult;
import com.example.githubproject.domain.service.*;
import com.example.githubproject.validation.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("/repos")
public class GitHubRestController {

    GitHubMapper gitHubMapper;
    GitHubService gitHubService;
    GitHubAdder gitHubAdder;
    GitHubDeleter gitHubDeleter;
    GitHubRetriever gitHubRetriever;
    GitHubUpdater gitHubUpdater;


    @GetMapping("/{user}")
    public ResponseEntity<GitHubListAllResult> getAllInfo(@PathVariable String user) {
        List<AllInfo> allInfoList = gitHubService.fetchAllInformation(user).allInfoList();
        gitHubService.databaseAdder(allInfoList);
        GitHubListAllResult response = new GitHubListAllResult(allInfoList);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{user}", headers = "Accept=application/xml")
    public ResponseEntity<ErrorResponse> wrongMediaType() {
        log.info("xml not use");
        ErrorResponse response = new ErrorResponse(HttpStatus.NOT_ACCEPTABLE, "Media type not acceptable");
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
    }


    @GetMapping("/database")
    public ResponseEntity<GitHubResultDatabaseList> getAllInfoFromDb() {
        List<GitHubResultDatabaseObject> gitHubResultDatabaseObjects = gitHubRetriever.findAll();
        GitHubResultDatabaseList gitHubResultDatabaseList = new GitHubResultDatabaseList(gitHubResultDatabaseObjects);

        return ResponseEntity.ok(gitHubResultDatabaseList);
    }

    @PostMapping
    public ResponseEntity<GitHubResultDatabaseObject> postRepo(@RequestBody GitHubResultDatabaseObject request) {
        gitHubAdder.addInfoToDb(request);
        return ResponseEntity.ok(request);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GitHubResultDatabaseObject> deleteRepo(@PathVariable Long id) {
        return ResponseEntity.ok(gitHubDeleter.deleteById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GitHubResultDatabaseObject> update(@PathVariable Long id, @RequestBody GitHubResultDatabaseObject request) {
        GitHubResultDatabaseObject gitToUpdate = new GitHubResultDatabaseObject(request.getName(), request.getOwner());
        gitHubUpdater.updateByID(id, gitToUpdate);
        return ResponseEntity.ok().build();
    }


}
