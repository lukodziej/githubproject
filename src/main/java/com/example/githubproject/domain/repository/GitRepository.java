package com.example.githubproject.domain.repository;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@Transactional
public interface GitRepository extends Repository<GitHubResultDatabaseObject,Long> {

    GitHubResultDatabaseObject save(GitHubResultDatabaseObject gitHubResult);

    List<GitHubResultDatabaseObject> findAll();

    GitHubResultDatabaseObject findById(Long id);

    GitHubResultDatabaseObject deleteById(Long id);

    @Modifying
    @Query("UPDATE GitHubResultDatabaseObject r SET r.name = :#{#newRepo.name}, r.owner = :#{#newRepo.owner} WHERE r.id = :id")
    void updateById(Long id, GitHubResultDatabaseObject newRepo);




}
