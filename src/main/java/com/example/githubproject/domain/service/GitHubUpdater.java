package com.example.githubproject.domain.service;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.repository.GitRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class GitHubUpdater {

    GitRepository gitRepository;

    GitHubRetriever gitHubRetriever;

    public GitHubUpdater(GitRepository gitRepository, GitHubRetriever gitHubRetriever) {
        this.gitRepository = gitRepository;
        this.gitHubRetriever = gitHubRetriever;
    }

    public void updateByID(Long id, GitHubResultDatabaseObject newRepo){
        gitHubRetriever.findById(id);
        log.info("Updating repo with id: " + id);
        gitRepository.updateById(id, newRepo);
    };
}
