package com.example.githubproject.domain.service;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.repository.GitRepository;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@Transactional
public class GitHubAdder {

    private final GitRepository gitRepository;


    public GitHubAdder(GitRepository gitRepository) {
        this.gitRepository = gitRepository;
    }

    public GitHubResultDatabaseObject addInfoToDb(GitHubResultDatabaseObject result){
        log.info("adding info to db");
        gitRepository.save(result);
        return result;
    }

}
