package com.example.githubproject.domain.service;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.proxy.*;
import com.example.githubproject.domain.proxy.dto.*;
import com.example.githubproject.validation.UserNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class GitHubService {

    GitHubServerProxy gitClient;

    GitHubMapper gitHubMapper;

    GitHubAdder gitHubAdder;

    public GitHubService(GitHubServerProxy gitClient, GitHubMapper gitHubMapper, GitHubAdder gitHubAdder) {
        this.gitClient = gitClient;
        this.gitHubMapper = gitHubMapper;
        this.gitHubAdder = gitHubAdder;
    }

    public List<GitHubResult> fetchAllRepos(String username) {
        try {
            String json = gitClient.makeGetRequest(username);
            return gitHubMapper.mapJsonToGitHubResultList(json)
                    .stream()
                    .filter(gitHubResult -> !gitHubResult.fork())
                    .toList();
        }catch (HttpClientErrorException ex){
            throw new UserNotFoundException("User: " + username + " not found" );
        }
    }

    public List<BranchResult> fetchAllBranches(String owner, String name) {
        String json = gitClient.makeGetBranchRequest(owner, name);
        return gitHubMapper.mapJsonToGitHubBranchResultList(json);
    }

    public GitHubListAllResult fetchAllInformation(String username) {
        List<AllInfo> allResults = new ArrayList<>();
        List<GitHubResult> gitHubResults = fetchAllRepos(username);
        for (GitHubResult result :
                gitHubResults) {
            List<BranchResult> branchResult = fetchAllBranches(result.owner().login(), result.name());
            AllInfo gitHubAllInformation = new AllInfo(result.name(), result.owner(), branchResult);
            allResults.add(gitHubAllInformation);
        }
        return new GitHubListAllResult(allResults);
    }

    public void databaseAdder(List<AllInfo> allInfoList){
            for (AllInfo info:
                    allInfoList) {
                gitHubAdder.addInfoToDb(new GitHubResultDatabaseObject(info.name(),info.owner().login()));
            }

    }
}
