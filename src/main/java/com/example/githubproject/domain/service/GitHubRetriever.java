package com.example.githubproject.domain.service;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.repository.GitRepository;
import com.example.githubproject.validation.IdNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class GitHubRetriever {

    private final GitRepository gitRepository;

    public GitHubRetriever(GitRepository gitRepository) {
        this.gitRepository = gitRepository;
    }

    public List<GitHubResultDatabaseObject> findAll(){
        log.info("retrieving all repos:");
        return gitRepository.findAll();
    }

    public GitHubResultDatabaseObject findById(Long id){
        log.info("finding by id: " + id );
        if (gitRepository.findById(id) == null) {
            throw new IdNotFoundException("Id: " + id + " not found");
        }
        return gitRepository.findById(id);
    }


}
