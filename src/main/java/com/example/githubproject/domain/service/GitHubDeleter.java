package com.example.githubproject.domain.service;

import com.example.githubproject.domain.model.GitHubResultDatabaseObject;
import com.example.githubproject.domain.repository.GitRepository;
import com.example.githubproject.validation.IdNotFoundException;
import com.example.githubproject.validation.UserNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
@Log4j2
public class GitHubDeleter {

    GitRepository gitRepository;

    GitHubRetriever gitHubRetriever;

    public GitHubDeleter(GitRepository gitRepository, GitHubRetriever gitHubRetriever) {
        this.gitRepository = gitRepository;
        this.gitHubRetriever = gitHubRetriever;
    }

    public GitHubResultDatabaseObject deleteById(Long id) {
        gitHubRetriever.findById(id);
        log.info("Deleting song with id: " + id);
        return gitRepository.deleteById(id);
    }


}
