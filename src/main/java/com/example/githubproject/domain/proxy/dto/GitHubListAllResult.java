package com.example.githubproject.domain.proxy.dto;

import java.util.List;

public record GitHubListAllResult(List<AllInfo> allInfoList) {
}
