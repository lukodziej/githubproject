package com.example.githubproject.domain.proxy.dto;

import java.util.List;


public record AllInfo(String name, Owner owner, List<BranchResult> branchResults) {
}
