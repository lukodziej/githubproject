package com.example.githubproject.domain.model;

import java.util.List;

public record GitHubResultDatabaseList(List<GitHubResultDatabaseObject> list) {
}
