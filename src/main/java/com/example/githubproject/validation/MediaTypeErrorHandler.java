package com.example.githubproject.validation;

import com.example.githubproject.infrastructure.controller.GitHubRestController;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Log4j2
@ControllerAdvice(assignableTypes = GitHubRestController.class)
public class MediaTypeErrorHandler {

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ResponseEntity<?> handleException(HttpMediaTypeNotAcceptableException exception) {
        log.error("Wrong Media Type!");
        return  new ResponseEntity<>(new ErrorResponse(exception.getStatusCode(), exception.getMessage()),exception.getStatusCode());
    }

}
