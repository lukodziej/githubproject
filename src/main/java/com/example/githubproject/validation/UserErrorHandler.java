package com.example.githubproject.validation;

import com.example.githubproject.infrastructure.controller.GitHubRestController;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Log4j2
@ControllerAdvice(assignableTypes = GitHubRestController.class)
public class UserErrorHandler {
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    @JsonSerialize()
    public ErrorResponse handleException(UserNotFoundException exception) {
        log.warn("USER NOT FOUND");
        return new ErrorResponse(exception.getStatusCode(), exception.getMessage());
    }

}
