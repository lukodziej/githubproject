package com.example.githubproject.validation;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

public class IdNotFoundException extends RuntimeException{


    public IdNotFoundException(String message) {
        super(message);
    }

    public HttpStatusCode getStatusCode() {
            return HttpStatus.NOT_FOUND;
        }
    }

