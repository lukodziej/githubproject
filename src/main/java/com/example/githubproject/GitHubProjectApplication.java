package com.example.githubproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class GitHubProjectApplication {

    @Autowired
    MainAppRunner mainAppRunner;

    public static void main(String[] args) {
        SpringApplication.run(GitHubProjectApplication.class, args);
    }


    @EventListener(ApplicationStartedEvent.class)
        public void run() {
            mainAppRunner.start();
        }
    }

